package com.rapidsoftcorp.automation;

/**
 * @author RapidSoft
 *
 */
public class EnhancedForLoop {

	public static void main(String[] args) {
		String[] names = { "Anand", "Ravi", "Raveendra", "Srinivas", "Sekhar" };

		// for(String name:names)
		// {
		// System.out.println(name);
		// }

		char grade = 'u';

		switch (grade) {
		case 'A':
			System.out.println("Excellent");
			break;
		case 'B':
			System.out.println("Good");
			break;
		case 'C':
			System.out.println("Can be better");
			break;
		case 'F':
			System.out.println("Sorry, failed");
			break;
		default:
			System.out.println("I dont know ");

		}

	}

}
