package com.rapidsoftcorp.automation;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ResourceLocators {

	WebDriver driver;

	@After
	public void stopWebDriver() {
		System.out.println("In @ After");
		driver.close();
	}

	@Before
	public void startWebDriver() {
		driver = new ChromeDriver();
		driver.get("http://newtours.demoaut.com/");
		driver.manage().window().maximize();
		System.out.println("In @ Before");
	}
	@Ignore 
	@Test
	public void testAWebDriverAPIs() throws InterruptedException {

		String pageTitle = driver.getTitle();
		System.out.println("pageTile" + pageTitle);
		if (pageTitle.equals("Welcome")) {
			System.out.println("You are right page");
		}

		driver.findElement(By.name("userName")).sendKeys("demo");
		driver.findElement(By.name("password")).sendKeys("demo");
		driver.findElement(By.name("login")).submit();
		System.out.println(driver.getTitle());
		
		List<WebElement> radioBtnList = driver.findElements(By.name("tripType"));
		
		for(WebElement radiobutton : radioBtnList)
		{
			
			System.out.println(radiobutton.getTagName());
			System.out.println(radiobutton.getAttribute("value"));
			if (radiobutton.getAttribute("value").equals("oneway"))
			{
				radiobutton.click();
			}
		// 9.28	
			
		}
		
		Thread.sleep(5000);
	}

	@Test
	public void testAWebbyXPATHs() throws InterruptedException {

		String pageTitle = driver.getTitle();
		System.out.println("pageTile" + pageTitle);
		if (pageTitle.equals("Welcome")) {
			System.out.println("You are right page");
		}

		
		
		driver.findElement(By.xpath("//input[@name='username']")).sendKeys("demo");;
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("demo");
		System.out.println(driver.getTitle());
		
		
		/*
		List<WebElement> radioBtnList = driver.findElements(By.name("tripType"));
		
		for(WebElement radiobutton : radioBtnList)
		{
			
			System.out.println(radiobutton.getTagName());
			System.out.println(radiobutton.getAttribute("value"));
			if (radiobutton.getAttribute("value").equals("oneway"))
			{
				radiobutton.click();
			}
		// 9.28	
			
		}
		
		Thread.sleep(5000);
		*/
	}

}
