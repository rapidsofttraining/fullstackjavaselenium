package com.rapidsoftcorp.automation;

public class Loops {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		int age = 1;

		while (age < 18) {

			if (age == 15) {
				System.out.println("doing a continue here");
				age++;
				continue;
			}

			if (age == 7) {
				System.out.println("doing a exit here");
				age++;
				break;
			}
			System.out.println("In loop " + age);
			age++;
		}
		System.out.println("here");
	}

}
