package com.rapidsoftcorp.automation;

/**
 * @author RapidSoft
 *
 */
public class BaseClass  {

	private String name;
	private String age;
	
	public String getName() {
		System.out.println("in baseclass getname");

		return name;
	}
	public void setName(String name) {
		System.out.println("in baseclass setname");
		System.out.println();
		this.name = name;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		System.out.println("in setAge");
		this.age = age;
	}
	
	public void setAge(int age) {
		
		this.age = String.valueOf(age);
	}
	

}
