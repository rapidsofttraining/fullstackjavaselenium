package com.rapidsoftcorp.automation;

import java.util.HashMap;
import java.util.Map.Entry;

public class HashMapDemo {
	
	public static void main(String[] args) {
		
		HashMap<String,String> makeAndModel = new HashMap<String,String>();
		
		makeAndModel.put("Toyota", "Camry");
		makeAndModel.put("Honda", "Accord");
		makeAndModel.put("BMW", "X5");
		makeAndModel.put("Subaru", "Outback");
		makeAndModel.put("Subaru", "Hatchback");
		makeAndModel.put(null, null);
		
		String model = makeAndModel.get("BMW");
		System.out.println("----" + model);
		
	//	Set keyset = makeAndModel.entrySet();
		
		for(Entry<String, String> carMandM : makeAndModel.entrySet())
				{
			
			    System.out.println("Make: "+ carMandM.getKey() + "Model: " +carMandM.getValue());
			
				}
		
		
		
		
		
	}
	

}
