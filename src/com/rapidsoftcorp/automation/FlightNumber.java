package com.rapidsoftcorp.automation;

public class FlightNumber {
	// one assumption is, last 4 chars can be digits,we want them only if digits
	// part
	// lets get last four characters only. Don't have to process full string
	// right?

	public static void main(String[] args) {
		String flightdetails = "The flight number is QA19";

		String flightnoonly = getFlightNumberOnly(flightdetails);
		System.out.println("This is final flight no " + flightnoonly);

	}

	private static String getFlightNumberOnly(String flightsentence) {
		int len = flightsentence.length();

		String string2process = flightsentence.substring(len - 4);
		string2process = string2process.trim();
		System.out.println(string2process);
		String final2return = null;
		System.out.println("This is substr to process " + string2process);
		for (int i = 0; i < string2process.length(); i++) {
			if (Character.isLetter(string2process.charAt(i))) {
				continue;
			} else {

				final2return = string2process.substring(i);
				break;
			}

		}

		return final2return;

	}

}
