package com.rapidsoftcorp.automation;

public interface Vehicle {
	
	public void start();
	public void stop();
	public void go();
}
