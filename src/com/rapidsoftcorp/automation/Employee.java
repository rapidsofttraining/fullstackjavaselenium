package com.rapidsoftcorp.automation;

/**
 * @author RapidSoft
 *
 */
public class Employee extends BaseClass {
	private String name;

	public String getSal() {
		System.out.println("in get Sal");
		return sal;
	}

	public void setSal(String sal) {
		System.out.println("in set Sal");
		this.sal = sal;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public void setName(String name) {
		System.out.println("in Employee setname");
		System.out.println();
		this.name = name;
	}

	public void setName(String name, String lastname) {
		System.out.println("in Employee setname");
		System.out.println();
		this.name = name;
	}

	private String sal;
	private String department;

}
