package com.rapidsoftcorp.automation;

/**
 * @author RapidSoft
 * 
 */
public class Artist {
	// two rules, name is same as classname, no return type
	private String name;
	private String profession;
	private static int count = 0;

	public static int getCount() {
		return count;
	}

	public Artist(String na, String prof) {
		name = na;
		profession = prof;
		count++;
	}

	public String getName() {
		return name;
	}

	public String getProfession() {
		return profession;
	}

}
