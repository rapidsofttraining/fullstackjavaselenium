package com.rapidsoftcorp.automation;

public class Car implements Vehicle {

	public void start() {

		System.out.println("starting car.....");
	}

	public void stop() {
		System.out.println("stoping car.....");

	}

	public void go() {
		System.out.println("go ......");

	}

	public static void main(String[] args) {

		Car mycar = new Car();
		mycar.start();
		mycar.go();
		mycar.stop();

	}

}
