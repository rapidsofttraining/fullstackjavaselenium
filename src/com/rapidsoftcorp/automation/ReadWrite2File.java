package com.rapidsoftcorp.automation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class ReadWrite2File {

	// 1 File
	// OutPut Stream
	// Input Stream

	public static void main(String[] args) {

		File myFile = new File("myFile.txt");
		// printWriter
		// scanner read from the file
		try {
			PrintWriter pw = new PrintWriter(myFile);
			pw.write("hello world");
			pw.write("how are you");
			pw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		try {
			Scanner reader = new Scanner(myFile);
			System.out.println("reader.nextLine =" + reader.nextLine());
			reader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}

}
