package com.rapidsoftcorp.automation;

import com.rapidsoftcorp.utils.Constants;

/**
 * @author RapidSoft
 *
 */
public class TestAutomation {

	// access modifier, private, public
	// return type String,int char
	// main entry point jvm looks for main
	// method signature
	public static void main(String[] args) {

		// String s = iWilLReturn();
		// String s1 = iWillTakeAndReturn("hi");
		// System.out.println(s1);
		// System.out.println(Constants.url);

		// BaseClass bc = new BaseClass(); // instance
		//
		// Employee emp = new Employee();
		// emp.setName("Kiran");
		// emp.setAge("12");
		// emp.setSal("2000");

		String name = "RapidSoft Training";
		int len = name.length();
		System.out.println(len);
		name = name.concat(" Tester");
		System.out.println(name);
		int indexval = name.indexOf("M");
		int indexvalEnd = name.indexOf("T");

		System.out.println(indexval);
		String substring = name.substring(indexval);
		System.out.println(substring.replace("Tester", "some value"));

		String substring1 = name.substring(indexval, indexvalEnd);
		System.out.println(substring1);

		String flightdetails = "Blue Skies Airlines 360";
		// assume 3 digit
		int lengofstring = flightdetails.length();
		System.out.println(flightdetails.substring(lengofstring - 3));
		// flightno89009
		// flightno890
		// 89009
		// 890
		int adultage = 19;
		int underage = 18;
		int retirmentage = 65;

		int age = 9;
		if (age > 18 && age < 65) {
			System.out.println("adult age");
		} else if (age > 65) {
			System.out.println("retriement age");
		}

		// switch

		// == (comparsion)

		// substring

		int num1 = 30;
		int num2 = 50;
		int restult = num1 * num2;
		System.out.println(restult);

		String skill = "Selenium";
		int j = skill.length();

		for (int i = 0; i < j; i++) {
			System.out.println(skill.charAt(i));
		}

		for (int i = 0; i < j; i++) {
			System.out.println(skill.charAt(i));
		}
		// out of bound expection
	}

	private void iWillNotReturn() {
		// class and objects class - blueprint object
	}

	private static String iWilLReturn() {

		return "take this";
	}

	private static String iWillTakeAndReturn(String takeThis) {
		return takeThis + " hello";
	}

}
