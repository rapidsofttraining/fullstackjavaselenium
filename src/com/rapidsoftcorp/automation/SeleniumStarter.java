package com.rapidsoftcorp.automation;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class SeleniumStarter {

	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome", "C:\\chrome\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://www.google.com");
		WebElement searchBox = driver.findElement(By.id("lst-ib"));
		searchBox.sendKeys("RapidSoft Training");
		searchBox.sendKeys(Keys.TAB);
		WebElement searchButton = driver.findElement(By.xpath("//*[@id='tsf']/div[2]/div[3]/center/input[1]"));
		searchButton.click();
		Thread.sleep(6000);
		String searchResult = driver.findElement(By.xpath("//*[@id='rso']/div/div/div[1]/div/div/h3/a")).getText();
		System.out.println("searchResult = " + searchResult);
		if ("RapidSoft Training: Home1".equals(searchResult)) {
			System.out.println("Found in the search results");

		} else {
			System.out.println("Not found in search results");
		}
		driver.close();
	}

}
