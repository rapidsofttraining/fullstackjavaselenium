package com.rapidsoftcorp.automation;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SeleniumJunitStarter {

	WebDriver driver;

	@After
	public void stopWebDriver() {
		System.out.println("In @ After");
		driver.close();
	}
	
	@Before
	public void startWebDriver() {
		driver = new ChromeDriver();
		driver.get("http://newtours.demoaut.com/");
		driver.manage().window().maximize();
		System.out.println("In @ Before");
	}

	@Test
	public void testAWebDriverAPIs() {

		String pageTitle = driver.getTitle();
		System.out.println("pageTile" + pageTitle);
		if (pageTitle.equals("Welcome")) {
			System.out.println("You are right page");
		}

		driver.findElement(By.name("userName")).sendKeys("demo");
		driver.findElement(By.name("password")).sendKeys("demo");
		driver.findElement(By.name("login")).submit();
	}

	@Test
	public void testAElementAPIs() {

	System.out.println("Title in testElement" + driver.getTitle());

	}
	
	@Test
	public void testANewAPIs() {

	System.out.println("Title in testANewAPIs" + driver.getTitle());

	}
	

	
	

}
