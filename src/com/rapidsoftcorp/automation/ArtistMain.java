package com.rapidsoftcorp.automation;

import java.util.ArrayList;

/**
 * @author RapidSoft
 *
 */
public class ArtistMain {

	public static void main(String[] args) {

		Artist art1 = new Artist("Rehman", "Music");
		Artist art2 = new Artist("Amitabh", "Actor");
		Artist art3 = new Artist("PCSreeram", "Photography");
		Artist art4 = new Artist("Vempati", "Dancer");

		// collections of artist into a collection -- ArrayList
		// java collections -
		ArrayList<Artist> ArtistList = new ArrayList<Artist>();

		ArtistList.add(art1);
		ArtistList.add(art2);
		ArtistList.add(art3);

		System.out.println(art1.getCount());

		// ArtistList
		// Enhanced foroop
		for (Artist arti : ArtistList) {
			System.out.println("Actor Name: " + arti.getName() + "Actor Profession " + arti.getProfession());

		}

		// for(int i=0; i<ArtistList.size(); i++)
		// {
		// System.out.println(ArtistList.get(i).getName());
		// }
		//
	}

}
